package com.virtualproz.library;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.ContentUris;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Typeface;
import android.media.ExifInterface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.provider.DocumentsContract;
import android.provider.MediaStore;
import android.provider.Settings;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.CursorLoader;
import android.text.Html;
import android.text.TextUtils;
import android.util.Base64;
import android.util.Log;
import android.util.TypedValue;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import com.google.gson.reflect.TypeToken;
import com.virtualproz.dao.Response;


import java.io.IOException;
import java.lang.reflect.Field;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;


public class Utils {
	private static String TAG = "Utils";

	public static String SHA256(String text){ // throws NoSuchAlgorithmException {

		try {
			MessageDigest md = MessageDigest.getInstance("SHA-256");

			md.update(text.getBytes());
			byte[] digest = md.digest();

			String result = Base64.encodeToString(digest, Base64.DEFAULT);
			return result;

		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		}

		Log.e(TAG, "SHA256 failed");
		return "";
	}
	public static String dump(Object object) {
		Field[] fields = object.getClass().getDeclaredFields();
		StringBuilder sb = new StringBuilder();
		sb.append(object.getClass().getSimpleName()).append('{');

		boolean firstRound = true;

		for (Field field : fields) {
			if (!firstRound) {
				sb.append(", ");
			}
			firstRound = false;
			field.setAccessible(true);
			try {
				final Object fieldObj = field.get(object);
				final String value;
				if (null == fieldObj) {
					value = "null";
				} else {
					value = fieldObj.toString();
				}

				//	            sb.append(field.getName()).append('=').append('\'')
				//	                    .append(value).append('\'');

				sb.append(field.getName()).append('=').append(value);

			} catch (IllegalAccessException e) {
				//this should never happen
				e.printStackTrace();
				Log.i(TAG, e.getMessage());
			}

		}

		sb.append('}');
		return sb.toString();
	}


	public static Typeface getTypeFace(Context context,String fontName)
	{
		String fontPath = "fonts/"+fontName;
		Typeface tf = Typeface.createFromAsset(context.getAssets(), fontPath);

		return tf;
	}



	public static void changeFonts(ViewGroup root,Context context,String fontName) {

		String fontPath = "fonts/"+fontName;

		try {
			Typeface tf = Typeface.createFromAsset(context.getAssets(), fontPath);

			for(int i = 0; i <root.getChildCount(); i++) {
				View v = root.getChildAt(i);
				if(v instanceof TextView ) {
					((TextView)v).setTypeface(tf);
				} else if(v instanceof Button) {
					((Button)v).setTypeface(tf);
				} else if(v instanceof EditText) {
					((EditText)v).setTypeface(tf);
				}
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public static void changeFonts(ViewGroup root,Context context) {

		try {
			Typeface tf = Typeface.createFromAsset(context.getAssets(), "fonts/helvetica-normal.ttf");

			for(int i = 0; i <root.getChildCount(); i++) {
				View v = root.getChildAt(i);
				if(v instanceof TextView ) {
					((TextView)v).setTypeface(tf);
				} else if(v instanceof Button) {
					((Button)v).setTypeface(tf);
				} else if(v instanceof EditText) {
					((EditText)v).setTypeface(tf);
				} else if(v instanceof ViewGroup) {
					changeFonts((ViewGroup)v,context);
				}
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public static void showInternetOptionDialog(final Context context) {
		// TODO Auto-generated method stub
		AlertDialog.Builder builder = new AlertDialog.Builder(context);
		builder.setTitle("Alert");  // GPS not found
		builder.setMessage("Please Enable Location Services"); // Want to enable?
		builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialogInterface, int i) {
				context.startActivity(new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS));
			}
		});
		builder.create().show();

	}

	public static String getCurrentISODate()
	{
		TimeZone tz = TimeZone.getTimeZone("UTC");
		DateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm'Z'");
		df.setTimeZone(tz);
		String nowAsISO = df.format(new Date());
		return nowAsISO;
	}



	public static int dpToPx(float dp, Resources resources){

		float px = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, resources.getDisplayMetrics());
		return (int) px;

	}
	public static int getRelativeTop(View myView) {
//	    if (myView.getParent() == myView.getRootView())
		if(myView.getId() == android.R.id.content)
			return myView.getTop();
		else
			return myView.getTop() + getRelativeTop((View) myView.getParent());

	}

	public static int getRelativeLeft(View myView) {
//	    if (myView.getParent() == myView.getRootView())
		if(myView.getId() == android.R.id.content)
			return myView.getLeft();
		else
			return myView.getLeft() + getRelativeLeft((View) myView.getParent());
	}

	public static boolean isEmailValid(CharSequence email) {
		return android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches();
	}


	public static boolean isLocationEnabled(Context context) {
		int locationMode = 0;
		String locationProviders;

		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT){
			try {
				locationMode = Settings.Secure.getInt(context.getContentResolver(), Settings.Secure.LOCATION_MODE);

			} catch (Settings.SettingNotFoundException e) {
				e.printStackTrace();
			}

			return locationMode != Settings.Secure.LOCATION_MODE_OFF;

		}else{
			locationProviders = Settings.Secure.getString(context.getContentResolver(), Settings.Secure.LOCATION_PROVIDERS_ALLOWED);
			return !TextUtils.isEmpty(locationProviders);
		}


	}


	public void replaceFonts(ViewGroup viewTree,Context context)
	{

		Typeface tf = Typeface.createFromAsset(context.getAssets(), "fonts/robotomedium.ttf");

		View child;
		for(int i = 0; i < viewTree.getChildCount(); ++i)
		{
			child = viewTree.getChildAt(i);
			if(child instanceof ViewGroup)
			{
				// recursive call
				replaceFonts((ViewGroup)child,context);
			}
			else if(child instanceof TextView)
			{
				// base case
				((TextView) child).setTypeface(tf);
			}
		}
	}









	public static boolean isInternetAvailible(Context context){

		ConnectivityManager connectivity = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
		if (connectivity != null)
		{
			NetworkInfo info = connectivity.getActiveNetworkInfo();
			if (info != null && info.isAvailable()) {
				return true;
			}
		}

		return false;
	}


	public static void showErrorSnackBar(View view, String message)
	{
		try {
			Snackbar snack = Snackbar.make(view, message, Snackbar.LENGTH_LONG)
                    .setAction("", null);
			ViewGroup group = (ViewGroup) snack.getView();
			group.setBackgroundColor(Color.WHITE);
			TextView tv = (TextView) group.findViewById(android.support.design.R.id.snackbar_text);
			tv.setTextColor(Color.BLACK);
			snack.show();
		} catch (Exception e) {
			e.printStackTrace();
		}


	}

	public static Bitmap rotateBitmapOrientation(String photoFilePath) {
		// Create and configure BitmapFactory
		BitmapFactory.Options bounds = new BitmapFactory.Options();
		bounds.inJustDecodeBounds = true;
		BitmapFactory.decodeFile(photoFilePath, bounds);
		BitmapFactory.Options opts = new BitmapFactory.Options();
		Bitmap bm = BitmapFactory.decodeFile(photoFilePath, opts);
		// Read EXIF Data
		ExifInterface exif = null;
		try {
			exif = new ExifInterface(photoFilePath);
		} catch (IOException e) {
			e.printStackTrace();
		}
		String orientString = exif.getAttribute(ExifInterface.TAG_ORIENTATION);
		int orientation = orientString != null ? Integer.parseInt(orientString) : ExifInterface.ORIENTATION_NORMAL;
		int rotationAngle = 0;
		if (orientation == ExifInterface.ORIENTATION_ROTATE_90) rotationAngle = 90;
		if (orientation == ExifInterface.ORIENTATION_ROTATE_180) rotationAngle = 180;
		if (orientation == ExifInterface.ORIENTATION_ROTATE_270) rotationAngle = 270;
		// Rotate Bitmap
		Matrix matrix = new Matrix();
		matrix.setRotate(rotationAngle, (float) bm.getWidth() / 2, (float) bm.getHeight() / 2);
		Bitmap rotatedBitmap = Bitmap.createBitmap(bm, 0, 0, bounds.outWidth, bounds.outHeight, matrix, true);
		// Return result
		return rotatedBitmap;
	}

	public static Bitmap rotateImage(Bitmap source, float angle) {
		Bitmap retVal;

		Matrix matrix = new Matrix();
		matrix.postRotate(angle);
		retVal = Bitmap.createBitmap(source, 0, 0, source.getWidth(), source.getHeight(), matrix, true);

		return retVal;
	}


	//////////////Permission Dialog ///////////
	public static void requestFineLocation(Activity activity ,int requestCode) {

		Log.i(TAG, "CAMERA permission has NOT been granted. Requesting permission.");
		// BEGIN_INCLUDE(camera_permission_request)
		if (ActivityCompat.shouldShowRequestPermissionRationale(activity,
				Manifest.permission.ACCESS_FINE_LOCATION)) {


			ActivityCompat.requestPermissions(activity,
					new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
					requestCode);

		} else {

			// Camera permission has not been granted yet. Request it directly.
			ActivityCompat.requestPermissions(activity, new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
					requestCode);
		}
		// END_INCLUDE(camera_permission_request)
	}


	public  static boolean isCameraPermission(Activity activity ,int requestCode) {

		if (Build.VERSION.SDK_INT >= 23) {
			if (activity.checkSelfPermission(Manifest.permission.CAMERA)
					== PackageManager.PERMISSION_GRANTED) {
				Log.v("TAG","Permission is granted");
				return true;
			} else {

				Log.v("TAG","Permission is revoked");
				ActivityCompat.requestPermissions(activity, new String[]{Manifest.permission.CAMERA,Manifest.permission.WRITE_EXTERNAL_STORAGE}, requestCode);
				return false;
			}
		}
		else { //permission is automatically granted on sdk<23 upon installation
			Log.v("TAG","Permission is granted");
			return true;
		}
	}


	public  static boolean isLocationPermission(Activity activity ,int requestCode) {

		if (Build.VERSION.SDK_INT >= 23) {
			if (activity.checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION)
					== PackageManager.PERMISSION_GRANTED) {
				Log.v("TAG","ACCESS FINE LOCATION Permission is granted");
				return true;
			} else {

				Log.v("TAG","Permission is revoked");
				ActivityCompat.requestPermissions(activity, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, requestCode);
				return false;
			}
		}
		else { //permission is automatically granted on sdk<23 upon installation
			Log.v("TAG","Permission is granted");
			return true;
		}
	}



	public  static  boolean isReadPermission(Activity activity,int requestCode) {

		if (Build.VERSION.SDK_INT >= 23) {
			if (activity.checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE)
					== PackageManager.PERMISSION_GRANTED) {
				Log.v("TAG","Permission is granted");
				return true;
			} else {

				Log.v("TAG","Permission is revoked");
				ActivityCompat.requestPermissions(activity, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, requestCode);
				return false;
			}
		}
		else { //permission is automatically granted on sdk<23 upon installation
			Log.v("TAG","Permission is granted");
			return true;
		}
	}

	public  static boolean isWritePerimisson(Activity activity ,int requestCode) {

		if (Build.VERSION.SDK_INT >= 23) {
			if (activity.checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE)
					== PackageManager.PERMISSION_GRANTED) {
				Log.v("TAG","Permission is granted");
				return true;
			} else {

				Log.v("TAG","Permission is revoked");
				ActivityCompat.requestPermissions(activity, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, requestCode);
				return false;
			}
		}
		else { //permission is automatically granted on sdk<23 upon installation
			Log.v("TAG","Permission is granted");
			return true;
		}
	}

	public  static boolean checkPermissionForMicrophone(Activity activity,int requestCode) {

		if (Build.VERSION.SDK_INT >= 23) {
			if (activity.checkSelfPermission(Manifest.permission.RECORD_AUDIO)
					== PackageManager.PERMISSION_GRANTED) {
				Log.v("TAG","Permission is granted");
				return true;
			} else {

				Log.v("TAG","Permission is revoked");
				ActivityCompat.requestPermissions(activity, new String[]{Manifest.permission.RECORD_AUDIO}, requestCode);
				return false;
			}
		}
		else { //permission is automatically granted on sdk<23 upon installation
			Log.v("TAG","Permission is granted");
			return true;
		}
	}


	public static void showToast(Context context, String message)
	{
		Toast.makeText(context,message,Toast.LENGTH_SHORT).show();
	}


	///Images Utils

	@SuppressLint("NewApi")
	public static String getRealPathFromURI_API19(Context context, Uri uri){
		String filePath = "";
		String wholeID = DocumentsContract.getDocumentId(uri);

		// Split at colon, use second item in the array
		String id = wholeID.split(":")[1];

		String[] column = { MediaStore.Images.Media.DATA };

		// where id is equal to
		String sel = MediaStore.Images.Media._ID + "=?";

		Cursor cursor = context.getContentResolver().query(MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
				column, sel, new String[]{ id }, null);

		int columnIndex = cursor.getColumnIndex(column[0]);

		if (cursor.moveToFirst()) {
			filePath = cursor.getString(columnIndex);
		}

		cursor.close();

		return filePath;
	}


	@SuppressLint("NewApi")
	public static String getRealPathFromURI_API11to18(Context context, Uri contentUri) {
		String[] proj = { MediaStore.Images.Media.DATA };
		String result = null;

		CursorLoader cursorLoader = new CursorLoader(
				context,
				contentUri, proj, null, null, null);
		Cursor cursor = cursorLoader.loadInBackground();

		if(cursor != null){
			int column_index =
					cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
			cursor.moveToFirst();
			result = cursor.getString(column_index);
		}

		return result;
	}

	public static String getRealPathFromURI_BelowAPI11(Context context, Uri contentUri){
		String[] proj = { MediaStore.Images.Media.DATA };
		Cursor cursor = context.getContentResolver().query(contentUri, proj, null, null, null);
		int column_index
				= cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
		cursor.moveToFirst();
		return cursor.getString(column_index);
	}



	 public static boolean isEmpty(EditText etText) {
		if (etText.getText().toString().trim().length() > 0)
			return false;

		return true;
	}






	//Get Image From URI Helper Functions


	public static String getImagePath(final Context context, final Uri uri) {

		final boolean isKitKat = Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT;

		// DocumentProvider
		if (isKitKat && DocumentsContract.isDocumentUri(context, uri)) {
			// ExternalStorageProvider
			if (isExternalStorageDocument(uri)) {
				final String docId = DocumentsContract.getDocumentId(uri);
				final String[] split = docId.split(":");
				final String type = split[0];

				if ("primary".equalsIgnoreCase(type)) {
					return Environment.getExternalStorageDirectory() + "/" + split[1];
				}

				// TODO handle non-primary volumes
			}
			// DownloadsProvider
			else if (isDownloadsDocument(uri)) {

				final String id = DocumentsContract.getDocumentId(uri);
				final Uri contentUri = ContentUris.withAppendedId(
						Uri.parse("content://downloads/public_downloads"), Long.valueOf(id));

				return getDataColumn(context, contentUri, null, null);
			}
			// MediaProvider
			else if (isMediaDocument(uri)) {
				final String docId = DocumentsContract.getDocumentId(uri);
				final String[] split = docId.split(":");
				final String type = split[0];

				Uri contentUri = null;
				if ("image".equals(type)) {
					contentUri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
				} else if ("video".equals(type)) {
					contentUri = MediaStore.Video.Media.EXTERNAL_CONTENT_URI;
				} else if ("audio".equals(type)) {
					contentUri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
				}

				final String selection = "_id=?";
				final String[] selectionArgs = new String[] {
						split[1]
				};

				return getDataColumn(context, contentUri, selection, selectionArgs);
			}
		}
		// MediaStore (and general)
		else if ("content".equalsIgnoreCase(uri.getScheme())) {

			// Return the remote address
			if (isGooglePhotosUri(uri))
				return uri.getLastPathSegment();

			return getDataColumn(context, uri, null, null);
		}
		// File
		else if ("file".equalsIgnoreCase(uri.getScheme())) {
			return uri.getPath();
		}

		return null;
	}


	public static String getDataColumn(Context context, Uri uri, String selection,
									   String[] selectionArgs) {

		Cursor cursor = null;
		final String column = "_data";
		final String[] projection = {
				column
		};

		try {
			cursor = context.getContentResolver().query(uri, projection, selection, selectionArgs,
					null);
			if (cursor != null && cursor.moveToFirst()) {
				final int index = cursor.getColumnIndexOrThrow(column);
				return cursor.getString(index);
			}
		} finally {
			if (cursor != null)
				cursor.close();
		}
		return null;
	}


	/**
	 * @param uri The Uri to check.
	 * @return Whether the Uri authority is ExternalStorageProvider.
	 */
	public static boolean isExternalStorageDocument(Uri uri) {
		return "com.android.externalstorage.documents".equals(uri.getAuthority());
	}

	/**
	 * @param uri The Uri to check.
	 * @return Whether the Uri authority is DownloadsProvider.
	 */
	public static boolean isDownloadsDocument(Uri uri) {
		return "com.android.providers.downloads.documents".equals(uri.getAuthority());
	}

	/**
	 * @param uri The Uri to check.
	 * @return Whether the Uri authority is MediaProvider.
	 */
	public static boolean isMediaDocument(Uri uri) {
		return "com.android.providers.media.documents".equals(uri.getAuthority());
	}

	/**
	 * @param uri The Uri to check.
	 * @return Whether the Uri authority is Google Photos.
	 */
	public static boolean isGooglePhotosUri(Uri uri) {
		return "com.google.android.apps.photos.content".equals(uri.getAuthority());
	}

	public static  void openWebThroughIntent(Activity activity,String Url)
	{

		Intent i = new Intent(Intent.ACTION_VIEW);
		i.setData(Uri.parse(Url));
		activity.startActivity(i);
	}

	public static void shareIntent(Context context ,String request,String heading,String message)
	{
		Intent sharingIntent = new Intent(Intent.ACTION_SEND);
		sharingIntent.setType("text/html");
		sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, Html.fromHtml("<b>"+heading+"</b><br><p>"+message+"</p>"));
		context.startActivity(Intent.createChooser(sharingIntent,request));
	}

}
