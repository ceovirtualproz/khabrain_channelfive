package com.virtualproz.library;

import android.content.Context;
import android.view.View;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.InterstitialAd;


public class AdManager {

	AdView adView;
	private InterstitialAd interstitial;
	Context context;
	View view;
	AdRequest adRequest;

	public AdManager(Context  context, String id)
	{
		this.context = context;
		interstitial = new InterstitialAd(context);
		interstitial.setAdUnitId(id);

		adRequest = new AdRequest.Builder()
				.build();


	}
	public void showBannerAdds(AdView add)
	{
		AdRequest adRequest = new AdRequest.Builder()
		.build();
		add.loadAd(adRequest);

	}
	public void displayInterstitial() {
		if (interstitial.isLoaded()) {
			interstitial.show();


		}
	}
	public void showInterstitialAdds()
	{

		interstitial.loadAd(adRequest);

		interstitial.setAdListener(new AdListener() {
			public void onAdLoaded() {
				displayInterstitial();
			}
		});
	}
}
