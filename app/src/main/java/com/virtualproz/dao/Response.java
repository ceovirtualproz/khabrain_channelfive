package com.virtualproz.dao;


import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class Response implements Serializable {

    @SerializedName("status")
    public int status;

    @SerializedName("new_user")
    public int new_user;

    @SerializedName("message")
    public String message;


    @SerializedName("verification_code")
    public String verification_code;


    @SerializedName("clientToken")
    public String clientToken;


    @SerializedName("user_data")
    public User user;



}