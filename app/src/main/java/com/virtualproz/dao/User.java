package com.virtualproz.dao;


import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class User implements Serializable {

    @SerializedName("result")
    public Address addressResult;

    public Address getAddressResult() {
        return addressResult;
    }

    public void setAddressResult(Address addressResult) {
        this.addressResult = addressResult;
    }


    @SerializedName("id")
    public String id;

    @SerializedName("phone")
    public String phone;

    @SerializedName("name")
    public String name;


    @SerializedName("address")
    public String address;


    @SerializedName("auth_token")
    public String auth_token;


    @SerializedName("latitude")
    public String latitude;


    @SerializedName("longitude")
    public String longitude;

    @SerializedName("type")
    public String type;

    @SerializedName("device_id")
    public String device_id;

    @SerializedName("gcm_key")
    public String gcm_key;


    @SerializedName("city")
    public String city;


    @SerializedName("state")
    public String state;



    @SerializedName("zipcode")
    public String zipcode;

    @SerializedName("orders")
    public String orders;


    @SerializedName("country")
    public String country;

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getZipcode() {
        return zipcode;
    }

    public void setZipcode(String zipcode) {
        this.zipcode = zipcode;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getDevice_id() {
        return device_id;
    }

    public void setDevice_id(String device_id) {
        this.device_id = device_id;
    }

    public String getGcm_key() {
        return gcm_key;
    }

    public void setGcm_key(String gcm_key) {
        this.gcm_key = gcm_key;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getLatitude() {
        return latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getOrders() {
        return orders;
    }

    public void setOrders(String orders) {
        this.orders = orders;
    }


    public String getAuth_token() {
        return auth_token;
    }

    public void setAuth_token(String auth_token) {
        this.auth_token = auth_token;
    }
}
