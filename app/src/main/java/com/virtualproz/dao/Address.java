package com.virtualproz.dao;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class Address implements Serializable{
	public String address1;
	public String address2;
	public String state;
	public String city;
	public String zipcode;
	public String lat;
	public String lang;



}
