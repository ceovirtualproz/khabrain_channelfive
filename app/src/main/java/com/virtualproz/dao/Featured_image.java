package com.virtualproz.dao;


import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Featured_image implements Serializable {


    @SerializedName("guid")
    public String guid;


    public String getGuid() {
        return guid;
    }

    public void setGuid(String rendered) {
        this.guid = rendered;
    }
}