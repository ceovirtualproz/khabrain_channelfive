package com.virtualproz.dao;


import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Program implements Serializable {


    @SerializedName("id")
    public String id;

    @SerializedName("heading")
    public String heading;

    @SerializedName("detail")
    public String detail;

    @SerializedName("image")
    public String image;

    @SerializedName("category")
    public String category;

    @SerializedName("created")
    public String created;

    @SerializedName("flag")
    public String flag;


    @SerializedName("name")
    public String name;


    @SerializedName("video_code")
    public String video_code;

    @SerializedName("is_active")
    public String is_active;



    @SerializedName("video_link")
    public String video_link;


    public String getVideo_link() {
        return video_link;
    }

    public void setVideo_link(String video_link) {
        this.video_link = video_link;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getVideo_code() {
        return video_code;
    }

    public void setVideo_code(String video_code) {
        this.video_code = video_code;
    }

    public String getIs_active() {
        return is_active;
    }

    public void setIs_active(String is_active) {
        this.is_active = is_active;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getHeading() {
        return heading;
    }

    public void setHeading(String heading) {
        this.heading = heading;
    }

    public String getDetail() {
        return detail;
    }

    public void setDetail(String detail) {
        this.detail = detail;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getCreated() {
        return created;
    }

    public void setCreated(String created) {
        this.created = created;
    }

    public String getFlag() {
        return flag;
    }

    public void setFlag(String flag) {
        this.flag = flag;
    }
}
