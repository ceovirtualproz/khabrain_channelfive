package com.virtualproz.dao;


import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Content implements Serializable {


    @SerializedName("rendered")
    public String rendered;


    public String getRendered() {
        return rendered;
    }

    public void setRendered(String rendered) {
        this.rendered = rendered;
    }
}