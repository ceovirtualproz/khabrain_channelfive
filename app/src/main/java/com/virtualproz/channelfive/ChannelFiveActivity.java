package com.virtualproz.channelfive;

import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.FrameLayout;
import android.widget.ListView;

import com.google.android.gms.ads.AdView;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.virtualproz.adapter.NewsAdapter;
import com.virtualproz.adapter.NewsVideoAdapter;
import com.virtualproz.adapter.ProgramAdapter;
import com.virtualproz.apiManager.ServiceGenerator;
import com.virtualproz.apiManager.UserInterface;
import com.virtualproz.apps.channel.five.khabrain.BaseActivity;

import com.virtualproz.apps.channel.five.khabrain.R;
import com.virtualproz.dao.News;
import com.virtualproz.library.AdManager;
import com.virtualproz.library.C;
import com.virtualproz.library.Utils;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ChannelFiveActivity extends BaseActivity {

    ListView listView;

    String category="international";
    Intent intent;


    AdManager adManager;

    List<News> newsList = new ArrayList<>();

    NewsAdapter adapter;
    FrameLayout btnBack;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chanelfive);

        initializeView();

    }


    public void initializeView()
    {
        listView = (ListView)findViewById(R.id.listview);
        btnBack = (FrameLayout)findViewById(R.id.btnBack);

        adView = (AdView)findViewById(R.id.adView);
        adManager = new AdManager(this, C.ADD_MOB_INERTIAL);
        adManager.showBannerAdds(adView);

        options = new DisplayImageOptions.Builder()
                .showImageOnLoading(R.drawable.chanel_five_header)
                .showImageForEmptyUri(R.drawable.chanel_five_header)
                .showImageOnFail(R.drawable.chanel_five_header).cacheInMemory(true)
                .cacheOnDisc(true).bitmapConfig(Bitmap.Config.RGB_565).build();

        intent = getIntent();

        if(intent!=null)
            category = (String)getIntent().getStringExtra(C.INTENT_CATEGORY);





        btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ChannelFiveActivity.this.finish();
            }
        });



        if(category!=null && category.equals(C.CATEGORY_HEADLINES)) {
            getHeadLines();
            intent = new Intent(ChannelFiveActivity.this,ChannelFiveVideoDetailActivity.class);

        }

        else if(category!=null && category.equals(C.CATEGORY_PROGRAM)) {
            getPrograms();
            intent = new Intent(ChannelFiveActivity.this,ChannelFiveProgramActivity.class);
        }


        else {
            intent = new Intent(ChannelFiveActivity.this,ChannelFiveDetailActivity.class);
            loadNews();
        }


        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {


                News news = (News)adapterView.getItemAtPosition(i);
                if(news != null)
                {

                    intent.putExtra(C.INTENT_NEWS ,news);
                    intent.putExtra(C.INTENT_VIDEO,news.getVideo_code());
                    startActivity(intent);

                }
            }
        });
    }

    void loadNews()
    {
        if(!Utils.isInternetAvailible(this)) {
            Utils.showToast(this,C.ERROR_INTERNET );
            return;
        }

        UserInterface service = ServiceGenerator.createServiceForChangelFive(UserInterface.class);

        Call<List<News>> call = service.getNews(category);
        showDialog();

        call.enqueue(new Callback<List<News>>() {
            @Override
            public void onResponse(Call<List<News>> call, Response<List<News>> response) {

                dissMissDialog();
                newsList = response.body();

                if(newsList!=null)
                {
                    adapter = new NewsAdapter(ChannelFiveActivity.this,newsList,imageLoader, options);
                    listView.setAdapter(adapter);

                }
            }

            @Override
            public void onFailure(Call<List<News>> call, Throwable t) {

                Utils.showToast(ChannelFiveActivity.this, t.getMessage());
                dissMissDialog();

            }
        });
    }


    void getHeadLines()
    {

        if(!Utils.isInternetAvailible(this)) {
            Utils.showToast(this,C.ERROR_INTERNET );
            return;
        }


        UserInterface service = ServiceGenerator.createLoginService(UserInterface.class);

        Call<List<News>> call = service.getHeadLines(category);
        showDialog();

        call.enqueue(new Callback<List<News>>() {
            @Override
            public void onResponse(Call<List<News>> call, Response<List<News>> response) {

                dissMissDialog();
                newsList = response.body();

                if(newsList!=null)
                {
                    NewsVideoAdapter adapter = new NewsVideoAdapter(ChannelFiveActivity.this,newsList,imageLoader, options);
                    listView.setAdapter(adapter);

                }
            }

            @Override
            public void onFailure(Call<List<News>> call, Throwable t) {

                Utils.showToast(ChannelFiveActivity.this, t.getMessage());
                dissMissDialog();

            }
        });
    }



    void getPrograms()
    {


        if(!Utils.isInternetAvailible(this)) {
            Utils.showToast(this,C.ERROR_INTERNET );
            return;
        }

        UserInterface service = ServiceGenerator.createServiceForChangelFive(UserInterface.class);

        Call<List<News>> call = service.getPrograms();
        showDialog();

        call.enqueue(new Callback<List<News>>() {
            @Override
            public void onResponse(Call<List<News>> call, Response<List<News>> response) {

                dissMissDialog();
                newsList = response.body();

                if(newsList !=null)
                {
                    ProgramAdapter adapter = new ProgramAdapter(ChannelFiveActivity.this, newsList,imageLoader, options);
                    listView.setAdapter(adapter);

                }
            }

            @Override
            public void onFailure(Call<List<News>> call, Throwable t) {

                Utils.showToast(ChannelFiveActivity.this, t.getMessage());
                dissMissDialog();
            }
        });
    }

}
