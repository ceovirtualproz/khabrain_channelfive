package com.virtualproz.channelfive;

import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.FrameLayout;
import android.widget.ListView;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.virtualproz.adapter.ProgramVideoAdapter;
import com.virtualproz.apiManager.ServiceGenerator;
import com.virtualproz.apiManager.UserInterface;
import com.virtualproz.apps.channel.five.khabrain.BaseActivity;
import com.virtualproz.apps.channel.five.khabrain.R;
import com.virtualproz.dao.News;
import com.virtualproz.dao.Program;
import com.virtualproz.library.C;
import com.virtualproz.library.Utils;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ChannelFiveProgramActivity extends BaseActivity {

    ListView listView;

    String category="international";
    Intent intent;


    List<News> programList = new ArrayList<>();

    ProgramVideoAdapter adapter;
    FrameLayout btnBack;

    News news;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chanelfive);

        initializeView();

    }


    public void initializeView()
    {
        listView = (ListView)findViewById(R.id.listview);
        btnBack = (FrameLayout)findViewById(R.id.btnBack);

        intent = getIntent();

        if(intent!=null)
            news = (News)intent.getSerializableExtra(C.INTENT_NEWS);


        options = new DisplayImageOptions.Builder()
                .showImageOnLoading(R.drawable.chanel_five_header)
                .showImageForEmptyUri(R.drawable.chanel_five_header)
                .showImageOnFail(R.drawable.chanel_five_header).cacheInMemory(true)
                .cacheOnDisc(true).bitmapConfig(Bitmap.Config.RGB_565).build();



        btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ChannelFiveProgramActivity.this.finish();
            }
        });



        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {


                News news = (News)adapterView.getItemAtPosition(i);
                if(news != null)
                {
                    intent = new Intent(ChannelFiveProgramActivity.this,ChannelFiveVideoDetailActivity.class);
                    intent.putExtra(C.INTENT_VIDEO,news.getVideo_link());
                    startActivity(intent);

                }
            }
        });



        getPrograms();




    }

    void getPrograms()
    {


        if(!Utils.isInternetAvailible(this)) {
            Utils.showToast(this,C.ERROR_INTERNET );
            return;
        }

        if(news==null)return;



        UserInterface service = ServiceGenerator.createLoginService(UserInterface.class);

        Call<List<News>> call = service.getProgramById(news.getId());
        showDialog();

        call.enqueue(new Callback<List<News>>() {
            @Override
            public void onResponse(Call<List<News>> call, Response<List<News>> response) {

                dissMissDialog();
                programList = response.body();

                if(programList !=null)
                {
                    adapter = new ProgramVideoAdapter(ChannelFiveProgramActivity.this, programList,imageLoader, options);
                    listView.setAdapter(adapter);

                }
            }

            @Override
            public void onFailure(Call<List<News>> call, Throwable t) {

                Utils.showToast(ChannelFiveProgramActivity.this, t.getMessage());
                dissMissDialog();

            }
        });
    }







}
