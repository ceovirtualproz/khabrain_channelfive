package com.virtualproz.channelfive;

import android.content.Intent;
import android.os.Bundle;
import android.text.Html;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.google.android.gms.ads.AdView;
import com.virtualproz.apiManager.ServiceGenerator;
import com.virtualproz.apiManager.UserInterface;
import com.virtualproz.apps.channel.five.khabrain.BaseActivity;

import com.virtualproz.apps.channel.five.khabrain.R;
import com.virtualproz.dao.News;
import com.virtualproz.library.AdManager;
import com.virtualproz.library.C;
import com.virtualproz.library.Utils;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ChannelFiveHomeActivity extends BaseActivity implements View.OnClickListener {

    FrameLayout btnLive,btnHeadlines,btnQomi,btnPrograms,btnWebsites,btnIntenationl,btnSports,btnShowbiz;
    Intent intent;

    TextView heading;

    FrameLayout btnBack;
    AdManager adManager;

    Button search;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_channelfive_home);
        initializeView();
    }


    public void initializeView()
    {

        btnLive = (FrameLayout)findViewById(R.id.btnLive);
        btnHeadlines = (FrameLayout)findViewById(R.id.btnHeadlines);
        btnQomi = (FrameLayout)findViewById(R.id.btnQomi);
        btnPrograms = (FrameLayout)findViewById(R.id.btnProgrames);
        btnWebsites = (FrameLayout)findViewById(R.id.btnWebsite);
        btnIntenationl = (FrameLayout)findViewById(R.id.btnInternationl);
        btnSports = (FrameLayout)findViewById(R.id.btnSprts);
        btnShowbiz = (FrameLayout)findViewById(R.id.btnShowbiz);
        heading = (TextView)findViewById(R.id.tickerHeading_txt) ;
        heading.setSelected(true);
        btnBack = (FrameLayout)findViewById(R.id.btnBack);

        adManager = new AdManager(this, C.ADD_MOB_INERTIAL);
        adView = (AdView)findViewById(R.id.adView);
        adManager.showBannerAdds(adView);


        search = (Button) findViewById(R.id.btnSearch);


        search.setOnClickListener(this);

        btnLive.setOnClickListener(this);
        btnHeadlines.setOnClickListener(this);
        btnQomi.setOnClickListener(this);
        btnPrograms.setOnClickListener(this);
        btnWebsites.setOnClickListener(this);
        btnIntenationl.setOnClickListener(this);
        btnSports.setOnClickListener(this);
        btnShowbiz.setOnClickListener(this);

        btnBack.setOnClickListener(this);
    }


    @Override
    public void onClick(View view)
    {

        if(view.equals(btnLive))
        {

            Utils.openWebThroughIntent(this,C.CATEGORY_LIVE);
        }

        else if(view.equals(btnHeadlines))
        {
            //moveActivity(C.CATEGORY_HEADLINES);

            Utils.openWebThroughIntent(this,C.CHANNELFIVE_HEADLINES);


        }

        else if(view.equals(btnQomi))
        {
            moveActivity(C.CATEGORY_NATIONAL);
        }
        else if(view.equals(btnPrograms))
        {
            //moveActivity(C.CATEGORY_PROGRAM);
            Utils.openWebThroughIntent(this,C.CHANNELFIVE_PROGRAMS);

        }

        else if(view.equals(btnWebsites))
        {
            Utils.openWebThroughIntent(this,C.CATEGORY_WEBSITE);
        }
        else if(view.equals(btnIntenationl))
        {

            moveActivity(C.CATEGORY_INTERNATIONAL);
        }

        else if(view.equals(btnSports))
        {

            moveActivity(C.CATEGORY_SPORTS);
        }

        else if(view.equals(btnShowbiz))
        {

            moveActivity(C.CATEGORY_SHOWBIZZ);
        }

        else if(view.equals(search))
        {

            Utils.openWebThroughIntent(this,C.E_PAPER_URL);
        }

        else if(view.equals(btnBack))
        {
            this.finish();
        }

    }

    public void moveActivity(String type)
    {
        intent = new Intent(ChannelFiveHomeActivity.this,ChannelFiveActivity.class);
        intent.putExtra(C.INTENT_CATEGORY , type);
        startActivity(intent);
        //this.finish();
    }


    @Override
    public  void onResume()
    {
        super.onResume();
        getTicker();
    }



    void getTicker()
    {


        if(!Utils.isInternetAvailible(this)) {
            Utils.showToast(this, C.ERROR_INTERNET );
            return;
        }

        UserInterface service = ServiceGenerator.createServiceForChangelFive(UserInterface.class);

        Call<List<News>> call = service.getTicker();

        call.enqueue(new Callback<List<News>>() {
            @Override
            public void onResponse(Call<List<News>> call, Response<List<News>> response) {
                String text="";
                List<News> list = response.body();

                if(list!=null)
                {
                    for(int i=0; i<list.size();i++)
                    {
                        if(list.get(i).getTitle()!=null)

                            text =    Html.fromHtml(list.get(i).getTitle()).toString();
                            ticker  = text +"           "+ ticker;

                    }

                }



                if(ticker!=null)
                    heading.setText(""+ticker);

                //ticker


            }

            @Override
            public void onFailure(Call<List<News>> call, Throwable t) {


            }
        });
    }





}
