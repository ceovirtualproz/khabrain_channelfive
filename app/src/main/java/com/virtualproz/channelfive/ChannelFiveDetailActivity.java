package com.virtualproz.channelfive;

import android.content.Intent;
import android.os.Bundle;
import android.text.Html;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.ImageLoader;
import com.virtualproz.apps.channel.five.khabrain.BaseActivity;

import com.virtualproz.apps.channel.five.khabrain.R;
import com.virtualproz.dao.News;
import com.virtualproz.library.C;
import com.virtualproz.library.Utils;

public class ChannelFiveDetailActivity extends BaseActivity {


    Intent intent;


    TextView heading, details,category;

    Button share;
    ImageView newsImage;

    FrameLayout btnBack;

    ImageLoader imageLoader = ImageLoader.getInstance();

    Button btnShare;

    News news;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_channelfive_detail);
        initializeView();
    }


    public void initializeView()
    {
        heading = (TextView)findViewById(R.id.headingtxt);
        details = (TextView)findViewById(R.id.detailtxt);
        category = (TextView)findViewById(R.id.categorytxt);
        btnShare = (Button)findViewById(R.id.btnShare);

        btnBack = (FrameLayout)findViewById(R.id.btnBack);

        newsImage = (ImageView)findViewById(R.id.newsImage);


        intent = getIntent();

        if(intent!=null)
            news = (News)intent.getSerializableExtra(C.INTENT_NEWS);


        if(news!=null)
        {

            heading.setText(news.getTitle().trim());
            details.setText((Html.fromHtml(news.getContent()).toString()));


            category.setText(news.getCategory());

            //imageLoader.displayImage(news.getImage(),newsImage,options);
            imageLoader.displayImage(news.getFeatured_image().getGuid(),newsImage,options);


        }

        btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                ChannelFiveDetailActivity.this.finish();
            }
        });


        btnShare = (Button)findViewById(R.id.btnShare);
        btnShare.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(news!=null)
                    Utils.shareIntent(ChannelFiveDetailActivity.this,"CHANNEL FIVE",news.getHeading(),news.getDetail());
            }
        });



    }


}
