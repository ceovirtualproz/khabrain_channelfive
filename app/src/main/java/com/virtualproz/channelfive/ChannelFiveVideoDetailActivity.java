package com.virtualproz.channelfive;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.ImageLoader;
import com.virtualproz.apps.channel.five.khabrain.BaseActivity;

import com.virtualproz.apps.channel.five.khabrain.R;
import com.virtualproz.dao.News;
import com.virtualproz.library.C;
import com.virtualproz.library.DMWebVideoView;

public class ChannelFiveVideoDetailActivity extends BaseActivity {


    Intent intent;


    TextView heading, details,category;

    Button share;
    ImageView newsImage;

    FrameLayout btnBack;

    ImageLoader imageLoader = ImageLoader.getInstance();

    String videoId;

    DMWebVideoView wv;
    WebSettings webSettings;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chanelfive_video_detail);

        initializeView();

    }


    public void initializeView()
    {
        btnBack = (FrameLayout)findViewById(R.id.btnBack);

        wv = (DMWebVideoView) findViewById(R.id.webview);
        wv.setAutoPlay(true);


        intent = getIntent();

        if(intent!=null)
            videoId = (String)intent.getStringExtra(C.INTENT_VIDEO);


        if(videoId!=null)
        {

            wv.setVideoId(videoId);
            wv.load();

        }




        btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                ChannelFiveVideoDetailActivity.this.finish();
            }
        });

    }


    @Override
    protected void onPause() {
        super.onPause();

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            wv.onPause();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            wv.onResume();
        }
    }


    @Override
    public void onBackPressed() {
        wv.handleBackPress(this);
    }


    void loadNews()
    {


    }



}
