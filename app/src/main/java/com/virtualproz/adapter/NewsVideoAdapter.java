package com.virtualproz.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.virtualproz.apps.channel.five.khabrain.R;
import com.virtualproz.dao.News;
import com.virtualproz.library.C;

import java.util.List;

public class NewsVideoAdapter extends BaseAdapter {

    String message1;
    private List<News> my_data ;
    private Context context;
    LayoutInflater inf ;
    DisplayImageOptions options;

    ImageLoader imageLoader;

    public NewsVideoAdapter(Context con, List<News> data  ,ImageLoader imageLoader,DisplayImageOptions options ) {

        my_data = data;
        context = con;
        options = options;
        this.imageLoader = imageLoader;

        inf = LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        return my_data.size();
    }

    @Override
    public Object getItem(int pos) {

        if(pos>=0 && pos< getCount())
            return my_data.get(pos);
        else
            return  null;
    }

    @Override
    public long getItemId(int arg0) {
        return 0;
    }

    @Override
    public View getView(final int pos, View convertView, final ViewGroup parent) {

        if(convertView==null){
            convertView =	inf.inflate(R.layout.row_news, parent, false);

        }
        ViewHolder holder = new ViewHolder();

        holder.news = (TextView)convertView.findViewById(R.id.newstxt);
        holder.icon = (ImageView)convertView.findViewById(R.id.newsImage);


        convertView.setTag(holder);


        News news = my_data.get(pos);

        holder.news.setText(news.getName().trim());
        imageLoader.displayImage(C.VIDEO_THUMB_URL+news.getVideo_code(),holder.icon ,options );


        return convertView;

    }


    static class ViewHolder {
        TextView news;
        ImageView icon;
    }


}
