package com.virtualproz.adapter;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import android.content.Context;
import android.telephony.SmsManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.virtualproz.apps.channel.five.khabrain.BaseActivity;
import com.virtualproz.apps.channel.five.khabrain.R;
import com.virtualproz.dao.News;

import org.w3c.dom.Text;

public class NewsAdapter extends BaseAdapter {

    String message1;
    private List<News> my_data ;
    private Context context;
    LayoutInflater inf ;
    DisplayImageOptions options;

    ImageLoader imageLoader;
    public NewsAdapter( Context con, List<News> data ,ImageLoader imageLoader,DisplayImageOptions options) {

        my_data = data;
        context = con;
        this.options = options;
        this.imageLoader = imageLoader;


        inf = LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        return my_data.size();
    }

    @Override
    public Object getItem(int pos) {

        if(pos>=0 && pos< getCount())
            return my_data.get(pos);
        else
            return  null;
    }

    @Override
    public long getItemId(int arg0) {
        return 0;
    }

    @Override
    public View getView(final int pos, View convertView, final ViewGroup parent) {

        if(convertView==null){
            convertView =	inf.inflate(R.layout.row_news, parent, false);

        }
        ViewHolder holder = new ViewHolder();

        holder.news = (TextView)convertView.findViewById(R.id.newstxt);
        holder.icon = (ImageView)convertView.findViewById(R.id.newsImage);


        convertView.setTag(holder);


        News news = my_data.get(pos);

        holder.news.setText(news.getTitle().trim());
//        holder.icon.setImageResource(R.drawable.ic_launcher);
        imageLoader.displayImage(news.getFeatured_image().getGuid(),holder.icon ,options );



        return convertView;

    }


    static class ViewHolder {
        TextView news;
        ImageView icon;
    }


}
