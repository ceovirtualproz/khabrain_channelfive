package com.virtualproz.apps.channel.five.khabrain;

import android.app.Dialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;

import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.MobileAds;
import com.nostra13.universalimageloader.cache.disc.impl.UnlimitedDiskCache;
import com.nostra13.universalimageloader.cache.disc.naming.HashCodeFileNameGenerator;
import com.nostra13.universalimageloader.cache.memory.impl.LruMemoryCache;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.QueueProcessingType;
import com.nostra13.universalimageloader.core.decode.BaseImageDecoder;
import com.nostra13.universalimageloader.core.download.BaseImageDownloader;
import com.nostra13.universalimageloader.utils.StorageUtils;
import com.virtualproz.adapter.ProgramAdapter;
import com.virtualproz.apiManager.ServiceGenerator;
import com.virtualproz.apiManager.UserInterface;
import com.virtualproz.channelfive.ChannelFiveActivity;
import com.virtualproz.dao.News;
//import com.virtualproz.library.AdManager;
import com.virtualproz.library.C;
import com.virtualproz.library.Utils;
import com.wang.avi.AVLoadingIndicatorView;

import java.io.File;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class BaseActivity extends AppCompatActivity {

	public SharedPreferences sharedPref;
	File cacheDir;
	ImageLoaderConfiguration config;
	int SCREEN_WIDTH = 0;
	int SCREEN_HEIGHT = 0;
	public DisplayImageOptions options;

	AVLoadingIndicatorView avi;
	Dialog dialog;

	public  ImageLoader imageLoader;

	//public  AdManager adManager;
	public  AdView adView;




	public  String ticker="";
	List<News> list;





	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);



		sharedPref = PreferenceManager
				.getDefaultSharedPreferences(this);

		cacheDir = StorageUtils.getCacheDirectory(this);

		//adManager = new AdManager(this);


//		config = new ImageLoaderConfiguration.Builder(this)
//				.memoryCacheExtraOptions(480, 800) // default = device screen dimensions
//				.diskCacheExtraOptions(480, 800, null)
//				.threadPriority(Thread.NORM_PRIORITY - 2) // default
//				.tasksProcessingOrder(QueueProcessingType.FIFO) // default
//				.denyCacheImageMultipleSizesInMemory()
//				.memoryCache(new LruMemoryCache(2 * 1024 * 1024))
//				.memoryCacheSize(2 * 1024 * 1024)
//				.memoryCacheSizePercentage(13) // default
//				.diskCache(new UnlimitedDiskCache(cacheDir)) // default
//				.diskCacheSize(50 * 1024 * 1024)
//				.diskCacheFileCount(100)
//				.diskCacheFileNameGenerator(new HashCodeFileNameGenerator()) // default
//				.imageDownloader(new BaseImageDownloader(this)) // default
//				.defaultDisplayImageOptions(DisplayImageOptions.createSimple()) // default
//				.writeDebugLogs()
//				.build();





		config = new ImageLoaderConfiguration.Builder(getApplicationContext())
				.memoryCacheExtraOptions(SCREEN_WIDTH, SCREEN_HEIGHT ) // default = device screen dimensions
				.diskCacheExtraOptions(SCREEN_WIDTH *2, SCREEN_HEIGHT*2, null)
				.threadPoolSize(3) // default
				.threadPriority(Thread.NORM_PRIORITY - 2) // default
				.tasksProcessingOrder(QueueProcessingType.FIFO) // default
				.denyCacheImageMultipleSizesInMemory()
				.memoryCache(new LruMemoryCache(2*1024*024))
				.memoryCacheSize(2*1024*1024)
				.memoryCacheSizePercentage(13) // default
				//   .diskCache(new UnlimitedDiscCache(cacheDir)) // default
				.diskCacheSize(100*1024*1024)
				.diskCacheFileCount(1000)
				.diskCacheFileNameGenerator(new HashCodeFileNameGenerator()) // default
				.imageDownloader(new BaseImageDownloader(getApplicationContext())) // default
				.defaultDisplayImageOptions(DisplayImageOptions.createSimple()) // default
				.writeDebugLogs()
				.build();

		options = new DisplayImageOptions.Builder()
				.showImageOnLoading(R.drawable.ic_khabrain_header)
				.showImageForEmptyUri(R.drawable.ic_khabrain_header)
				.showImageOnFail(R.drawable.ic_khabrain_header).cacheInMemory(true)
				.cacheOnDisc(true).bitmapConfig(Bitmap.Config.RGB_565).build();
		ImageLoader.getInstance().init(config);

		imageLoader = ImageLoader.getInstance();

	}

	public void showDialog() {

		dialog = new Dialog(this, R.style.dialog_full_screen);
		dialog.setContentView(R.layout.dialog_loading);

		avi = (AVLoadingIndicatorView)dialog.findViewById(R.id.avi);
		avi.show();

		dialog.setCancelable(true);
		dialog.show();

	}
//
	public void dissMissDialog() {

		if(avi!=null)
			if(avi.isShown())
				avi.hide();

		if (dialog != null) {
			dialog.dismiss();
		}

	}






}