package com.virtualproz.apps.channel.five.khabrain;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.view.menu.ActionMenuItemView;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;

import com.crashlytics.android.Crashlytics;
import com.google.android.gms.ads.MobileAds;
import com.virtualproz.channelfive.ChannelFiveActivity;
import com.virtualproz.channelfive.ChannelFiveHomeActivity;
import com.virtualproz.khbarain.KhabrainHomeActivity;
import com.virtualproz.library.C;

import io.fabric.sdk.android.Fabric;

public class SplashActivity extends AppCompatActivity {


    FrameLayout btnKhabrian, btnChannelfive;

    Intent intent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Fabric.with(this, new Crashlytics());
        MobileAds.initialize(getApplicationContext(), C.ADDMOB_APP_ID);

        setContentView(R.layout.splash_activity);



        btnChannelfive = (FrameLayout)findViewById(R.id.btnChannelfive);
        btnKhabrian = (FrameLayout)findViewById(R.id.btnKhabrian);


        btnChannelfive.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                intent = new Intent(SplashActivity.this, ChannelFiveHomeActivity.class);
                startActivity(intent);
            }
        });

        btnKhabrian.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                intent = new Intent(SplashActivity.this, KhabrainHomeActivity.class);
                startActivity(intent);


            }
        });

    }
}
