package com.virtualproz.khbarain;

import android.content.Intent;
import android.os.Bundle;
import android.text.Html;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.crashlytics.android.Crashlytics;
import com.google.android.gms.ads.AdView;
import com.virtualproz.apiManager.ServiceGenerator;
import com.virtualproz.apiManager.UserInterface;
import com.virtualproz.apps.channel.five.khabrain.BaseActivity;
import com.virtualproz.apps.channel.five.khabrain.R;
import com.virtualproz.dao.News;
import com.virtualproz.library.AdManager;
import com.virtualproz.library.C;
import com.virtualproz.library.Utils;

import java.util.List;

import io.fabric.sdk.android.Fabric;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class KhabrainHomeActivity extends BaseActivity implements View.OnClickListener {

    FrameLayout btnBainUlAqwami, btnKhail,btnIntertaiment,btnMagazin,btnKalam,btnQomi;
    Intent intent;

    FrameLayout btnBack;

    Button ePaper;
    TextView heading;

    ImageView banner;
    AdManager adManager;





    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_khabrain_home);

        initializeView();

    }


    public void initializeView()
    {

        btnBainUlAqwami = (FrameLayout)findViewById(R.id.btnBainulaqvami);
        btnKhail = (FrameLayout)findViewById(R.id.btnKhail);
        btnIntertaiment = (FrameLayout)findViewById(R.id.btnIntertainment);
        btnMagazin = (FrameLayout)findViewById(R.id.btnMagazin);
        btnKalam = (FrameLayout)findViewById(R.id.btnKalam);
        btnQomi = (FrameLayout)findViewById(R.id.btnQomi);
        btnBack = (FrameLayout)findViewById(R.id.btnBack);
        ePaper = (Button)findViewById(R.id.btnEpaper);
        heading = (TextView)findViewById(R.id.tickerHeading_txt) ;
        heading.setSelected(true);

        banner = (ImageView)findViewById(R.id.banner);


        adView = (AdView)findViewById(R.id.adView);
        adManager = new AdManager(this, C.ADD_MOB_INERTIAL);
        adManager.showBannerAdds(adView);


        imageLoader.displayImage(C.BANNER_URL,banner);


        ePaper.setOnClickListener(this);

        btnBainUlAqwami.setOnClickListener(this);
        btnKalam.setOnClickListener(this);
        btnQomi.setOnClickListener(this);
        btnKhail.setOnClickListener(this);
        btnIntertaiment.setOnClickListener(this);
        btnMagazin.setOnClickListener(this);

        btnBack.setOnClickListener(this);
    }


    @Override
    public void onClick(View view)
    {

        if(view.equals(btnBainUlAqwami))
        {

            moveActivity(C.CATEGORY_INTERNATIONAL);
        }

        else if(view.equals(btnQomi))
        {
            moveActivity(C.CATEGORY_PAKISTAN);
        }

        else if(view.equals(btnKhail))
        {
            moveActivity(C.CATEGORY_SPORTS);
        }
        else if(view.equals(btnIntertaiment))
        {
            moveActivity(C.CATEGORY_SHOWBIZZ);
        }

        else if(view.equals(btnKalam))
        {
            Utils.openWebThroughIntent(this,C.KALAM_URL);
        }
        else if(view.equals(btnMagazin))
        {

            Utils.openWebThroughIntent(this,C.MAGZINE_URL);
        }
        else if(view.equals(ePaper))
        {

            Utils.openWebThroughIntent(this,C.E_PAPER_URL);
        }

        else if(view.equals(btnBack))
        {
            this.finish();
        }

    }

    public void moveActivity(String type)
    {
        intent = new Intent(KhabrainHomeActivity.this,KhabrainActivity.class);
        intent.putExtra(C.INTENT_CATEGORY , type);
        startActivity(intent);
        //this.finish();

    }

    void getTicker()
    {


        if(!Utils.isInternetAvailible(this)) {
            Utils.showToast(this, C.ERROR_INTERNET );
            return;
        }

        UserInterface service = ServiceGenerator.createLoginService(UserInterface.class);

        Call<List<News>> call = service.getTicker();

        call.enqueue(new Callback<List<News>>() {
            @Override
            public void onResponse(Call<List<News>> call, Response<List<News>> response) {

                List<News> list = response.body();

                String text ="";
                if(list!=null)
                {
                    for(int i=0; i<list.size();i++)
                    {
                        if(list.get(i).getTitle()!=null)

                            text =    Html.fromHtml(list.get(i).getTitle()).toString();
                            ticker  = text+"           "+ ticker;
                    }
                }

                if(ticker!=null)
                    heading.setText(""+ticker);

                //ticker


            }

            @Override
            public void onFailure(Call<List<News>> call, Throwable t) {


            }
        });
    }

    @Override
    public  void onResume()
    {
        super.onResume();
        getTicker();
    }



}
