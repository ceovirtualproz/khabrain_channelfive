package com.virtualproz.khbarain;

import android.content.Intent;
import android.os.Bundle;
import android.os.IBinder;
import android.view.View;
import android.widget.AdapterView;
import android.widget.FrameLayout;
import android.widget.ListView;

import com.google.android.gms.ads.AdView;
import com.virtualproz.adapter.NewsAdapter;
import com.virtualproz.apiManager.ServiceGenerator;
import com.virtualproz.apiManager.UserInterface;
import com.virtualproz.apps.channel.five.khabrain.BaseActivity;
import com.virtualproz.apps.channel.five.khabrain.R;
import com.virtualproz.dao.News;
import com.virtualproz.library.AdManager;
import com.virtualproz.library.C;
import com.virtualproz.library.Utils;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class KhabrainActivity extends BaseActivity  {

    ListView listView;

    String category="international";
    Intent intent;
    List<News> newsList = new ArrayList<>();

    NewsAdapter adapter;
    FrameLayout btnBack;


    AdManager adManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_khabrain);

        initializeView();



    }


    public void initializeView()
    {
        listView = (ListView)findViewById(R.id.listview);
        btnBack = (FrameLayout)findViewById(R.id.btnBack);
        adView = (AdView)findViewById(R.id.adView);
        adManager = new AdManager(this, C.ADD_MOB_INERTIAL);
        adManager.showBannerAdds(adView);


        intent = getIntent();

        if(intent!=null)
            category = (String)getIntent().getStringExtra(C.INTENT_CATEGORY);

//        intent = new Intent(ChannelFiveActivity.this,KhabrainDetailActivity.class);
//        startActivity(intent);



        loadNews();



        btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                KhabrainActivity.this.finish();
            }
        });

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {


                News news = (News)adapterView.getItemAtPosition(i);
                if(news != null)
                {

                    intent = new Intent(KhabrainActivity.this,KhabrainDetailActivity.class);
                    intent.putExtra(C.INTENT_NEWS ,news);
                    startActivity(intent);

                }
            }
        });

    }

    void loadNews()
    {

        if(!Utils.isInternetAvailible(this)) {
            Utils.showToast(this,C.ERROR_INTERNET );
            return;
        }


        UserInterface service = ServiceGenerator.createLoginService(UserInterface.class);

        Call<List<News>> call = service.getNews(category);
        showDialog();

        call.enqueue(new Callback<List<News>>() {
            @Override
            public void onResponse(Call<List<News>> call, Response<List<News>> response) {

                dissMissDialog();
                newsList = response.body();

                if(newsList!=null)
                {
                    adapter = new NewsAdapter(KhabrainActivity.this,newsList,imageLoader,options);
                    listView.setAdapter(adapter);

                }
            }

            @Override
            public void onFailure(Call<List<News>> call, Throwable t) {

                Utils.showToast(KhabrainActivity.this, t.getMessage());
                dissMissDialog();

            }
        });
    }





}
