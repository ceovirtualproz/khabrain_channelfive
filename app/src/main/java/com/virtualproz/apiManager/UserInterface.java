package com.virtualproz.apiManager;



import com.virtualproz.dao.News;
import com.virtualproz.dao.Program;
import com.virtualproz.dao.Response;
import com.virtualproz.dao.User;
import com.virtualproz.library.C;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.PartMap;
import retrofit2.http.Path;
import retrofit2.http.Query;


public interface UserInterface {

    @GET("posts")
    Call<List<News>> getNews(@Query("filter[category_name]") String category);

    @GET("/{id}")
    Call<List<News>> getHeadLines(@Path("id") String category);


    @GET("getdata/programs")
    Call<List<News>> getPrograms();

    @GET("getdata/programs/{id}")
    Call<List<News>> getProgramById(@Path("id") String id);




    @GET("posts")
    Call<List<News>> getTicker();





}


